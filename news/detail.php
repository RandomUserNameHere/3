<?/**@var $APPLICATION*/
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Главная страница тестового задания");
?>


<div class="content centered">
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","news",Array(
            "START_FROM" => "0",
            "PATH" => "",
            "SITE_ID" => "s1"
        )
    );?>

    <?$APPLICATION->IncludeComponent(
        "bitrix:news.detail",
        "news",
        Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "USE_SHARE" => "N",
            "SHARE_HIDE" => "Y",
            "SHARE_TEMPLATE" => "",
            "SHARE_HANDLERS" => array(),
            "SHARE_SHORTEN_URL_LOGIN" => "",
            "SHARE_SHORTEN_URL_KEY" => "",
            "AJAX_MODE" => "Y",
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => "2",
            "ELEMENT_CODE" => $_REQUEST["CODE"],
            "CHECK_DATES" => "Y",
            "FIELD_CODE" => Array(),
            "PROPERTY_CODE" => Array(),
            "IBLOCK_URL" => "/news/",
            "DETAIL_URL" => "",
            "SET_TITLE" => "Y",
            "SET_CANONICAL_URL" => "Y",
            "SET_BROWSER_TITLE" => "Y",
            "BROWSER_TITLE" => "",
            "SET_META_KEYWORDS" => "Y",
            "META_KEYWORDS" => "",
            "SET_META_DESCRIPTION" => "Y",
            "META_DESCRIPTION" => "",
            "SET_LAST_MODIFIED" => "Y",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            "ADD_SECTIONS_CHAIN" => "Y",
            "ADD_ELEMENT_CHAIN" => "Y",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "USE_PERMISSIONS" => "Y",
            "GROUP_PERMISSIONS" => Array("1"),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Страница",
            "PAGER_TEMPLATE" => "",
            "PAGER_SHOW_ALL" => "N ",
            "PAGER_BASE_LINK_ENABLE" => "Y",
            "SET_STATUS_404" => "Y",
            "SHOW_404" => "Y",
            "MESSAGE_404" => "",
            "PAGER_BASE_LINK" => "",
            "PAGER_PARAMS_NAME" => "arrPager",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
        )
    );?>

    <?$APPLICATION->IncludeComponent(
        'web136:similar.news',
        'similar',
        array(
            'IBLOCK_ID'=>2,
            'NEWS_CODE'=>$_REQUEST["CODE"],
            'NEWS_COUNT'=>5
        )
    );?>
</div>

<?require  $_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php";?>