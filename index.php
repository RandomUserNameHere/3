<? /**@var $APPLICATION */ ?>
<?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Главная страница тестового задания");
?>

<div class="content centered">
    <? $APPLICATION->IncludeComponent("bitrix:main.include", "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/content_page/index_text.php"
        )
    ); ?>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>