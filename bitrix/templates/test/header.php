<?/**@var $APPLICATION*/?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html>
<head>
    <title><?$APPLICATION->ShowTitle();?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/template_styles.css"

    <?$APPLICATION->ShowMeta("keywords")?>
    <?$APPLICATION->ShowMeta("description")?>

    <?$APPLICATION->ShowCSS();?>
    <?$APPLICATION->ShowHeadScripts();?>
    <?$APPLICATION->ShowHeadStrings();?>


</head>
<body>
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div class="content-holder">
        <header>
            <?$APPLICATION->IncludeComponent("bitrix:menu","main.menu",Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
        </header>

