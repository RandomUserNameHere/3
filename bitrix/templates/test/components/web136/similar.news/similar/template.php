<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die(); ?>


<? if (!empty($arResult['SIMILAR_NEWS'])): ?>
    <div class="similar-holder">
    <div class="similar-heading">Похожие новости</div>
    <? foreach ($arResult['SIMILAR_NEWS'] as $id => $item): ?>
        <div class="similar-item">
            <div>
                <a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a>
            </div>
            <div>
                <? /*=$item['PREVIEW_TEXT']*/ ?>
            </div>
        </div>
    <? endforeach; ?>
    </div>
<? endif; ?>

