<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();

    //Собираем справочник ID видоа спорта для выборки

    $sprotsIdList = array();

    foreach ($arResult['ITEMS'] as $item) {
        foreach ($item['PROPERTY_1'] as $property){
            if(!empty($property)){
                $sprotsIdList[] = $property;
            }
        }
    }

    //Получаем данные по ИД в свойствах

    CModule::IncludeModule('iblock');

    $res = CIBlockElement::GetList(
        array("SORT"=>'ASC'),
        array("IBLOCK_ID"=>1,
              "ID"=>$sprotsIdList,
              "ACTIVE"=>"Y"
        ),
        FALSE,
        FALSE,
        array(
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_PAGE_URL'
        )
    );

    $arSports = array();

    while($item = $res->GetNext(TRUE, FALSE)){
        $arSports[$item['ID']] = $item;
    }

    //Заносим данные в результат

    foreach ($arResult['ITEMS'] as $key => $item) {
        foreach ($item['PROPERTY_1'] as $index => $property){
            if(!empty($property)){
                $arResult['ITEMS'][$key]['SPORTS'][$property] = $arSports[$property];
            }
        }
    }




