<? /**@var $arResult */ ?>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die(); ?>

<? if (!empty($arResult['ITEMS'])): ?>
    <div class="newslist">
        <? foreach ($arResult['ITEMS'] as $item): ?>
            <div class="news-item">
                <div class="news-item-heading">
                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a>
                </div>
                <div class="news-preview-holder">
                    <div class="prevew-image-holder">
                        <? if (!empty($item['PREVIEW_PICTURE'])): ?>
                            <img src="<?= $item['PREVIEW_PICTURE'] ?>" alt="<?= $item['NAME'] ?>">
                        <? endif; ?>
                    </div>
                    <div class="preview-text-holder">
                        <? /*=$item['PREVIEW_TEXT']*/ ?>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? else: ?>
    <div>Новостей пока нет</div>
<? endif; ?>