<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?/* if(!empty($arResult)):*/?><!--
    <ul class="breadcrumb">
        <li><a href="<?/*=SITE_SERVER_NAME*/?>">Главная</a></li>
        <?/* foreach($arResult as $item):*/?>
            <li>
                <?/* if($item['LINK']):*/?>
                    <a href="<?/*=$item['LINK']*/?>"><?/*=$item['TITLE']*/?></a>
                <?/* else: */?>
                    <span><?/*=$item['TITLE']*/?></span>
                <?/* endif; */?>
            </li>
        <?/* endforeach; */?>
    </ul>
--><?/* endif; */?>

<?php


    /**
     * @global CMain $APPLICATION
     */

    global $APPLICATION;

    //delayed function must return a string
    if(empty($arResult))
        return "";

    $strReturn = '';

    $strReturn .= '<ul class="breadcrumb">';
    $strReturn .= "<li><a href='/index.php'>Главная</a></li>";

    foreach($arResult as $item){
        $strReturn .= '<li>';
            if($item['LINK']){
                $strReturn .= "<a href='{$item['LINK']}'>{$item['TITLE']}</a>";
            }else{
                $strReturn .= "<span>{$item['TITLE']}</span>";
            }
        $strReturn .= '</li>';
    }


    $strReturn .= '</ul>';

    return $strReturn;

