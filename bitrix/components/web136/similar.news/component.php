<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();

    /**@var $arParams array */

    //Установка значений по умолчанию
    if (empty(intval($arParams['IBLOCK_ID'], 10)) && empty(trim($arParams['IBLOCK_CODE']))) {
        ShowError("Не могу определить инфоблок");
        return;
    }


    if (empty(intval($arParams['NEWS_ID'], 10)) && empty(trim($arParams['NEWS_CODE']))){
        ShowError("Не могу определить новость");
        return;
    }

    if (empty($arParams['NEWS_COUNT'])) {
        $arParams['NEWS_COUNT'] = 4;
    }

    if (!isset($arParams['CACHE_TIME'])) {
        $arParams["CACHE_TIME"] = 36000000;
    }

    $arResult = array();

    if ($this->StartResultCache($arParams['CACHE_TIME'])) {

        if (!CModule::IncludeModule("iblock")) {
            $this->AbortResultCache();
            ShowError("Модуль iblock не установлен");

            return;
        }



        //Собираем массив группировки
        $arOder = array();

        if(!empty(trim($arParams['SORT1']))){
            if(empty(trim($arParams['SORT1_ORDER']))){
                $arParams['SORT1_ORDER'] = 'DESC';
            }

            $arOder[$arParams['SORT1']] = $arParams['SORT1_ORDER'];
        }

        if(!empty(trim($arParams['SORT2']))){
            if(empty(trim($arParams['SORT2_ORDER']))){
                $arParams['SORT2_ORDER'] = 'ASC';
            }
            $arOder[$arParams['SORT2']] = $arParams['SORT2_ORDER'];
        }

        //Собираем фильтр для получения свойств новости
        $arNewsFilter = array(
            'IBLOCK_ACTIVE' => 'Y'
        );

        if(isset($arParams['IBLOCK_ID'])&&!empty($arParams['IBLOCK_ID'])){
            $arNewsFilter['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
        }
        else{
            $arNewsFilter['IBLOCK_CODE'] = $arParams['IBLOCK_CODE']; 
        }

        if(isset($arParams['NEWS_ID'])&&!empty($arParams['NEWS_ID'])){
            $arNewsFilter['ID'] = $arParams['NEWS_ID'];
        }
        else{
            $arNewsFilter['CODE'] = $arParams['NEWS_CODE'];
        }




        //Получаем из новости значения привязанных элементов для фильтра

        $res = CIBlockElement::GetList(
            array("SORT"=>"ASC"),
            $arNewsFilter,
            false,
            array(
                'nTopCount'=>1
            ),
            array(
                'ID',
                'IBLOCK_ID',
                'PROPERTY_SPORTS'
            )
        );

        $arCurrentNews = array();

        while($item = $res->Fetch()){
            $arCurrentNews = $item;
        }


        //Фильтр для отбора похожих новостей
        $arFilter = array(
            'IBLOCK_ACTIVE' => 'Y',//Активный инфоблок
            'ACTIVE' => 'Y',//Активная новость
            "<=DATE_ACTIVE_FROM" => array(false, ConvertTimeStamp(false, "FULL")),//Уже активна
            ">=DATE_ACTIVE_TO"   => array(false, ConvertTimeStamp(false, "FULL")),//Еще активна
            'PROPERTY_SPORTS' => $arCurrentNews['PROPERTY_SPORTS_VALUE'],//Похожие виды спорта
            'IBLOCK_ID'=>$arCurrentNews['IBLOCK_ID'],
            '!ID'=>$arCurrentNews['ID']
        );

        $similarRes = CIBlockElement::GetList(
            $arOder,
            $arFilter,
            false,
            array(
                'nTopCount'=>$arParams['NEWS_COUNT']
            ),
            array(
                'IBLOCK_ID',
                'ID',
                'NAME',
                'CODE',
                'DETAIL_PAGE_URL',
                'DETAIL_PICTURE',
                'DETAIL_TEXT',
                'PREVIEW_TEXT'

            )
        );

        while($similarNews = $similarRes->GetNext(true, false)){
            $arResult['SIMILAR_NEWS'][$similarNews['ID']] = $similarNews;
        }

        /*echo '<pre>';
        print_r($arResult);
        echo '</pre>';*/


        //Подключаем шаблон
        $this->IncludeComponentTemplate();
    }



