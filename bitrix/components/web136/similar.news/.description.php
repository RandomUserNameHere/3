<?
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    $arComponentDescription = array(
        "NAME" => "Похожие новости по видам спорта",
        "DESCRIPTION" => "Новости по видам спорта",
        "ICON" => "/images/news_detail.gif",
        "SORT" => 30,
        "CACHE_PATH" => "Y",
        "PATH" => array(
            "ID" => "content"
        ),
    );