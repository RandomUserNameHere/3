<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();

    if (!CModule::IncludeModule("iblock")){return;}

    $arComponentParameters = array(
        'IBLOCK_ID' => array(
            'NAME' => "ID инфоблока",
            'TYPE' => 'INTEGER',
            'MULTIPLE' => 'N'
        ),
        'IBLOCK_CODE' => array(
            'NAME' => 'Символьный код инфоблока',
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N'
        ),
        'NEWS_ID' => array(
            'NAME' => "ID новости для вывода похожих",
            'TYPE' => 'INTEGER',
            'MULTIPLE' => 'N'
        ),
        'NEWS_CODE' => array(
            'NAME' => 'Символьный код новости для вывода похожих',
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N'
        ),
        'NEWS_COUNT' => array(
            'NAME' => "Количество новостей для вывода",
            'TYPE' => 'INTEGER',
            'MULTIPLE' => 'N',
            'DEFAULT' => 4
        ),
        'SORT1'=>array(
            'NAME'=>'Поле первой сортировки',
            'TYPE'=>'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'ACTIVE_FROM'
        ),
        'SORT1_ORDER'=>array(
            'NAME'=>'Порядок первой сортировки',
            'TYPE'=>'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'DESC'
        ),
        'SORT2'=>array(
            'NAME'=>'Поле второй сортировки',
            'TYPE'=>'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'SORT'
        ),
        'SORT2_ORDER'=>array(
            'NAME'=>'Порядок второй сортировки',
            'TYPE'=>'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'ASC'
        ),
        'CACHE_TIME' => array('DEFAULT' => 3600)
    );
