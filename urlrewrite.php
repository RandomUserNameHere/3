<?
    $arUrlRewrite = array(
        array(
            "CONDITION" => "#^\/news\/([0-9A-Za-z_-]+)\/{0,1}\?*(.*)$#",
            "RULE" => "CODE=\$1&\$2",
            "ID" => "",
            "PATH" => "/news/detail.php",
        ),

        array(
            "CONDITION" => "#^\/sports\/([0-9A-Za-z_-]+)\/{0,1}\?*(.*)$#",
            "RULE" => "CODE=\$1&\$2",
            "ID" => "",
            "PATH" => "/sports/detail.php",
        )
    );
